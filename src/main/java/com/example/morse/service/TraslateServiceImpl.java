package com.example.morse.service;

import com.example.morse.interfaces.MorseTraslate;
import com.example.morse.interfaces.TraslateService;
import com.example.morse.intities.Traslate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TraslateServiceImpl implements TraslateService {

    private Traslate traslateObj;
    @Autowired
    private MorseTraslate traslate;

    @Override
    public Traslate getMorseTraslate(String text,Integer type) {
        traslateObj = new Traslate();
        if(type==1){
            traslateObj.setMorse(traslate.translate2Human(text));
        }else{
            traslateObj.setAlfabetico(traslate.alfabeticTraslate(text));
        }

        return traslateObj;
    }
}
