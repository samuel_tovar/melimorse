package com.example.morse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MorseApplication {

    public static void main(String[] args) {
        int b = 10;
        int v = (--b -9 + ++b * b++);
        System.out.println(b);
        System.out.println(--b -9);
        System.out.println(++b * b++);

        System.out.println(v);
        SpringApplication.run(MorseApplication.class, args);
    }

}
