package com.example.morse.interfaces;

public interface MorseTraslate {
      public String alfabeticTraslate(String text);
      public String translate2Human(String morse);
}
