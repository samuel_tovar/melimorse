package com.example.morse.intities;

import org.springframework.context.annotation.Bean;


public class Traslate {
    String morse;
    String alfabetico;

    public Traslate() {
    }

    public Traslate(String morse, String alfabetico) {
        this.morse = morse;
        this.alfabetico = alfabetico;
    }

    public String getMorse() {
        return morse;
    }

    public void setMorse(String morse) {
        this.morse = morse;
    }

    public String getAlfabetico() {
        return alfabetico;
    }

    public void setAlfabetico(String alfabetico) {
        this.alfabetico = alfabetico;
    }
}
