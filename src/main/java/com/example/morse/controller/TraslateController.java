package com.example.morse.controller;

import com.example.morse.interfaces.TraslateService;
import com.example.morse.intities.Traslate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/traslate")
public class TraslateController {
    @Autowired
    private TraslateService traslateService;

    @ResponseBody
    @RequestMapping(value = "/2text", method = RequestMethod.POST)
    public String morse2(@RequestParam("text") String name) {
        return traslateService.getMorseTraslate(name,1).getMorse();
    }

    @ResponseBody
    @RequestMapping(value = "/2morse", method = RequestMethod.POST)
    public String text2(@RequestParam("text") String name) {
        return traslateService.getMorseTraslate(name,2).getAlfabetico();
    }

}
